import { Component } from '@angular/core';
import { evaluate, unaryMinus, square, nthRoot } from 'mathjs';
import { CalcHistoryService } from '../history/calc-history.service';
import { LogEntry } from '../history/LogEntry.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  symbol: string;
  private logEntry: LogEntry = {
    cache: null,
    symbol: '',
    input: null,
    result: null
  };

  equation: string = '';
  sum: number = 0;
  input: string = '';
  private done: boolean = false;

  calcHistory: Array<[string, number]> = [];

  constructor(private calcHistoryService: CalcHistoryService) { }

  typeIn(value: string) {
    //falls Eingabe = Zahl
    if (!isNaN(Number(value))) {
      if (this.done) {
        this.input = '';
        this.done = false;
      }
      this.input += value;
    }
    else {
      // falls Eingabe = Komma
      if (value === ".") {
        console.log("Komma");
        console.log(this.input.indexOf(value));
        if (this.input.indexOf(value) != -1)
          return;

        if (this.input.length === 0)
          this.input = "0.";
        else {
          this.input += '.';
        }
        console.log(this.input);
      }

      else {
        // Eingabe Ergebnis anzeigen
        if (value === "=") {
          if (this.logEntry.cache != null
            && this.logEntry.symbol != ""
            && this.input != "") {
            console.log("Ergebnis");
            this.result();
          } else { return };
        }
        // Eingabe = Rechenoperation
        else {
          console.log("Rechenoperation");
          console.log("current entry: " + JSON.stringify(this.logEntry));
          if (!this.done) {
            this.evaluate();
            this.logEntry.cache = this.logEntry.result;
            this.logEntry.input = null;
            this.logEntry.result = null;
          }
          if (this.input === "" || parseFloat(this.input) == 0) {
            console.log("kein input da");
            this.logEntry.cache = 0;
          }

          else if (this.logEntry.cache === null) {
            console.log("kein cache vorhanden");
            this.logEntry.cache = parseFloat(this.input);
          }

          this.logEntry.symbol = value;
          this.done = true;
          this.equation = this.logEntry.cache + " " + this.logEntry.symbol;
        }
      }
    }
  }

  evaluate() {
    if (this.logEntry.cache != null &&
      this.logEntry.symbol != "" &&
      this.input != "" &&
      !this.done) {
      try {
        this.equation = this.logEntry.cache + " " + this.logEntry.symbol;
        this.logEntry.input = parseFloat(this.input);
        this.logEntry.result = evaluate(this.equation + this.input);
        this.calcHistoryService.addEntry(this.logEntry.cache, this.logEntry.symbol, this.logEntry.input, this.logEntry.result);
      }
      catch (e) {
        console.error("error", e.message);
      }
      console.log('---\n' + this.input + '\n' + this.sum + '\n' + this.equation);
    }
  }

  result() {
    this.evaluate();
    this.equation = this.logEntry.cache + " " + this.logEntry.symbol + " " + this.logEntry.input + " =";
    this.input = this.logEntry.result.toString();
    this.logEntry.cache = this.logEntry.result;
    this.logEntry.result = null;
    this.done = true;
    this.logEntry.symbol = "";
  }

  clear() {
    this.equation = "";
    this.logEntry = {
      cache: null,
      symbol: '',
      input: null,
      result: null
    };
    this.input = "";
  }

  negate() {
    this.input = unaryMinus(this.input);
    if (this.done)
      this.done = false;
  }

  percent() {
    if (this.logEntry.cache != null && this.logEntry.symbol != "") {
      this.input = ((this.logEntry.cache / 100.0) * parseFloat(this.input)).toString();
      this.evaluate();
      this.equation = this.logEntry.cache + " " + this.logEntry.symbol + " " + this.logEntry.input + " =";
      this.input = this.logEntry.result.toString();
      this.logEntry.symbol = "";
      this.done = true;
      this.logEntry.cache = this.logEntry.result;
      this.logEntry.result = null;
    }
  }

  reciprocal() {
    if (this.logEntry.cache != null && this.logEntry.symbol != "") {
      this.input = (1 / parseFloat(this.input)).toString();
      this.evaluate();
      this.equation = this.logEntry.cache + " " + this.logEntry.symbol + " " + this.logEntry.input + " =";
      this.input = this.logEntry.result.toString();
      this.logEntry.symbol = "";
      this.done = true;
      this.logEntry.cache = this.logEntry.result;
      this.logEntry.result = null;
    }
  }

  square() {
    if (this.input === "") { return }
    else {
      if (this.logEntry.cache != null && this.logEntry.symbol != "") {
        this.input = square(parseFloat(this.input)).toString();
        this.evaluate();
        this.equation = this.logEntry.cache + " " + this.logEntry.symbol + " " + this.logEntry.input + " =";
        this.input = this.logEntry.result.toString();
        this.logEntry.symbol = "";
        this.done = true;
        this.logEntry.cache = this.logEntry.result;
        this.logEntry.result = null;
      }
      else {
        this.logEntry.symbol = "²";
        this.logEntry.cache = parseFloat(this.input);
        this.logEntry.input = 0;
        this.logEntry.result = square(parseFloat(this.input));
        this.calcHistoryService.addEntry(this.logEntry.cache, this.logEntry.symbol, this.logEntry.input, this.logEntry.result);
        this.equation = this.input + " ² =";
        this.input = this.logEntry.result.toString();
        this.logEntry.symbol = "";
        this.done = true;
        this.logEntry.cache = this.logEntry.result;
        this.logEntry.result = null;
      }
    }
  }

  sqRoot() {
    if (this.input === "") { return }
    else {
      if (this.logEntry.cache != null && this.logEntry.symbol != "") {
        this.input = nthRoot(parseFloat(this.input)).toString();
        this.evaluate();
        this.equation = this.logEntry.cache + " " + this.logEntry.symbol + " " + this.logEntry.input + " =";
        this.input = this.logEntry.result.toString();
        this.logEntry.symbol = "";
        this.done = true;
        this.logEntry.cache = this.logEntry.result;
        this.logEntry.result = null;
      }
      else {
        this.logEntry.symbol = "√";
        this.logEntry.cache = 2;
        this.logEntry.input = parseFloat(this.input);
        this.logEntry.result = nthRoot(parseFloat(this.input));
        this.calcHistoryService.addEntry(this.logEntry.cache, this.logEntry.symbol, this.logEntry.input, this.logEntry.result);
        this.equation = " √( " + this.input + " ) =";
        this.input = this.logEntry.result.toString();
        this.logEntry.symbol = "";
        this.done = true;
        this.logEntry.cache = this.logEntry.result;
        this.logEntry.result = null;
      }
    }
  }

  delete() {
    if (this.input.length > 0)
      this.input = this.input.slice(0, -1);
  }

}
