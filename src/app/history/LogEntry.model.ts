export interface LogEntry{
  cache: number;
  symbol: string;
  input: number;
  result: number;
}
