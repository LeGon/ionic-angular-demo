import { TestBed } from '@angular/core/testing';

import { CalcHistoryService } from './calc-history.service';

describe('CalcHistoryService', () => {
  let service: CalcHistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CalcHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
