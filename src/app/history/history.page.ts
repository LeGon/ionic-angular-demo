import { Component, OnInit } from '@angular/core';
import {Router } from '@angular/router';

import { CalcHistoryService } from './calc-history.service';
import { LogEntry } from './LogEntry.model';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  history: LogEntry[];

  constructor(private calcHistoryService: CalcHistoryService, private router:Router) { }

  ngOnInit() {
    this.history = this.calcHistoryService.getAllEntries();
  }

  onDeleteEntry(entry: LogEntry) {
    this.calcHistoryService.deleteEntry(entry);
    this.history= this.calcHistoryService.getAllEntries();
  }

}
