import { Injectable } from '@angular/core';
import { LogEntry } from './LogEntry.model';

@Injectable({
  providedIn: 'root'
})
export class CalcHistoryService {
  private history: LogEntry[] = [];

  constructor() { }

  addEntry(cache:number, symbol:string, input:number, result:number){
    this.history.push({cache,symbol,input,result});
    console.log("Entry added to history");
  }

  getAllEntries(){
    return this.history;
  }

  deleteEntry(deleteItem:LogEntry){
    this.history = this.history.filter(entry => {
      return entry !== deleteItem;
    })
    console.log("delete Entry: " + JSON.stringify(deleteItem));
  }
}
